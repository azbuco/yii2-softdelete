# Usage #

Add the behaviour to the model


```
#!php
<?php
use azbuco\softdelete\behaviors\SoftDelete;

public function behaviors()
{
    return [
        [
            'class' => SoftDelete::className(),
        ],
    ];
}

```

Add the action to the controller


```
#!php
<?php
use azbuco\softdelete\actions\SoftDelete;

public function actions()
{
    return [
         // Restore a soft-deleted model
         'restore' => [
            'class' => SoftDelete::className(),
            'findModel' => function($id) {
                return $this->findModel($id);
            }
        ],
        // Delete a model. 
        // This action tries to force-delete the model, and if it fails soft-delete it.
        'delete' => [
            'class' => SoftDelete::className(),
            'action' => SoftDelete::ACTION_DELETE,
            'findModel' => function($id) {
                return $this->findModel($id);
            }
        ]
    ];
}
```