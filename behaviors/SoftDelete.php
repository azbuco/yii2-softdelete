<?php

namespace azbuco\softdelete\behaviors;

use yii\behaviors\TimestampBehavior;
use yii\base\Event;
use yii\db\ActiveRecord;

/**
 * SoftDelete
 * 
 * public function behaviors() {
 *     return [
 *         'softDelete' => ['class' => 'azbuco\softdelete\behaviors\SoftDelete',
 *             'deletedAttribute' => 'torolve',
 *             'timestamp' => time(),
 *             'value' => new ,
 *         ],
 *     ];
 * }
 */
class SoftDelete extends TimestampBehavior {

    /**
     * @var string A mező ami a soft delete értékét tartalmazza
     */
    public $deletedAttribute = "torolve";

    /**
     * @var bool A soft delete engedélyezése. Ha false, a delete normál módon működik
     */
    public $safeMode = true;
    
    /**
     *
     * @var 
     */
    public $useDateTime = true;

    /**
     * @inheritdoc
     */
    public function events() {
        return [ActiveRecord::EVENT_BEFORE_DELETE => 'doDeleteTimestamp'];
    }

    /**
     * Set the attribute with the current timestamp to mark as deleted
     *
     * @param Event $event
     */
    public function doDeleteTimestamp($event) {

        // do nothing if safeMode is disabled. this will result in a normal deletion
        if (!$this->safeMode) {
            return;
        }

        // remove and mark as invalid to prevent real deletion
        $result = $this->remove();
        $event->isValid = false;
        return $result;
    }

    /**
     * Remove (soft-delete) record
     */
    public function remove() {
        // evaluate timestamp and set attribute
        $value = $this->useDateTime ? new \yii\db\Expression('NOW()') : $this->getValue(null);
        
        $attribute = $this->deletedAttribute;
        $this->owner->$attribute = $value;

        // save record
        return $this->owner->save(false, [$attribute]);
    }

    /**
     * Egy törölt elem visszaállítása
     */
    public function restore() {
        // mark attribute as null
        $attribute = $this->deletedAttribute;
        $this->owner->$attribute = null;

        // save record
        return $this->owner->save(false, [$attribute]);
    }

    /**
     * Mindenképp töröl egy elemet az adatbázisbóül
     */
    public function forceDelete() {
        // store model so that we can detach the behavior and delete as normal
        $model = $this->owner;
        $this->detach();
        return $model->delete();
    }
    
    /**
     * A model töröltként van-e megjelölve
     * 
     * @return boolean
     */
    public function isDeleted() {
        $attribute = $this->deletedAttribute;
        return !empty($this->owner->$attribute);
    }
}