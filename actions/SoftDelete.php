<?php

namespace azbuco\softdelete\actions;

use Closure;
use Yii;
use yii\base\InvalidConfigException;
use yii\db\IntegrityException;
use yii\helpers\Inflector;
use yii\helpers\Url;
use yii\rest\Action;

class SoftDelete extends Action
{
    const ACTION_RESTORE = 'restoreAction';
    const ACTION_DELETE = 'deleteAction';
    const ACTION_FORCE_DELETE = 'forceDeleteAction';

    public $action = null;
    public $findModel = null;
    public $redirect = null;

    public function init()
    {
        

        if ($this->findModel === null) {
            throw new InvalidConfigException('The findModel configuration property is required for ' . __CLASS__);
        }
    }

    public function run($id)
    {
        $model = $this->findModel instanceof Closure ?
            call_user_func($this->findModel, $id) :
            $this->findModel($id);

        if ($this->action === null) {
            $callback = [$this, Inflector::variablize($this->id) . 'Action'];
        } else {
            $callback = is_array($this->action) ? $this->action : [$this, $this->action];
        }
        
        call_user_func($callback, $model);
    }

    public function deleteAction($model)
    {
        $model->delete();
        $this->redirect();
    }

    public function forceDeleteAction($model)
    {
        if (!$model->forceDelete()) {
            Yii::$app->getSession()->setFlash('error', 'Nem lehet törölni az elemet', false);
        }

        $this->redirect();
    }

    public function restoreAction($model)
    {
        if (!$model->restore()) {
            Yii::$app->getSession()->setFlash('error', 'Nem lehet visszaállítani az elemet', false);
        }

        $this->redirect();
    }

    public function redirect()
    {
        $to = $this->redirect ? $this->redirect : Yii::$app->request->referrer;
        if ($to === null) {
            $to = Url::to('[index]');
        }

        if (Yii::$app->request->isPjax) {
            Yii::$app->response->headers->set('Location', $to);
        }

        Yii::$app->controller->redirect($to);
    }
}
